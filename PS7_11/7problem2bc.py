import numpy as np
import matplotlib.pyplot as plt

import load_img
import density
import fit_gauss
import cloud_mov
import constants as cst
from table import print_table

def main():
    y0s, sigmas = cloud_mov.cloud_expansion(cst.FILENAMES)
    y0y = cloud_mov.get_ypos(cst.FILENAMES)
    
    print_table([cst.T, ['s ']*len(cst.T), y0s, ['m ']*len(y0s), y0y, ['m ']*len(y0y)])
    print('maximum difference in y0: {}'.format(max(abs(a - b) for (a, b) in zip(y0s,y0y))))
    
    plt.plot(cst.T, y0s, cst.T, y0y)
    plt.legend(['2d fit', '1d fit'])
    plt.show()

    plt.plot(cst.T, [x/y for (x, y) in sigmas])
    plt.show()

if __name__ == '__main__':
    main()
