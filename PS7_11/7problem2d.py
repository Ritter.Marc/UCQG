import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

import cloud_mov
import constants as cst
from table import print_eq

def sigma_t(t, *p):
    w0, rho = p
    return np.sqrt(w0**2 + t**2 * rho**2)
    #fit alg will try negative parameters => imaginary sqrt;
    #therefore use $\rho$ as parameter: $\rho := \sqrt{k_B T / m}$

def Temp_from_slope(rho):
    return cst.M_RB * rho**2 / cst.K_B

def main():
    T2 = np.linspace(0, cst.T_MAX, 201)
    
    y0s, sigmas = cloud_mov.cloud_expansion(cst.FILENAMES)
    sigma_x, sigma_y = zip(*sigmas)
    
    fit = [curve_fit(sigma_t, cst.T, s, p0=(min(s), (s[-1] - s[-2])/(cst.T[-1] - cst.T[-2])))[0]
            for s in (sigma_x, sigma_y)]

    print_eq(['T_x', 'T_y'], [Temp_from_slope(f[1]) for f in fit], ['K'] * 2)

    plt.plot(cst.T, sigma_x, 'b-', 
            T2,  sigma_t(T2, *fit[0]), 'b--',
            cst.T, sigma_y, 'r-',
            T2, sigma_t(T2, *fit[1]), 'r--')
    plt.legend(['sigma_x', 'fit', 'sigma_y', 'fit'])
    plt.show()

if __name__ == '__main__':
    main()
