import numpy as np
import matplotlib.pyplot as plt

import constants as cst
import load_img
import density
import fit_gauss_2d
from table import print_table

def main():
    anums_fit = []
    for name in cst.FILENAMES:
        n = density.n_column(load_img.load_image(name), cst.SIGMA_RB)

        fit = fit_gauss_2d.fit_gauss2d(cst.GRID_X, cst.GRID_Y, n)
        #   $N = 2\pi n_0 \sigma_x \sigma_y$
        anums_fit.append(2*np.pi * fit[0] * fit[1] * fit[2])

    print_table([['time'] + cst.T, [''] + ['s '] * len(cst.T),
        ['apparent number of atoms'] + list(anums_fit)])
    
    plt.plot(cst.T, anums_fit)
    plt.show()

if __name__ == '__main__':
    main()
