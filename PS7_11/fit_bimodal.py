import numpy as np
from scipy.optimize import curve_fit

import fit_gauss_2d
from fit_gauss import get_FWHM
from density import integrate_equidistant
from table import print_eq

def extract_tf_p(p):
    return (p[6], p[1]*p[7], p[2]*p[8], p[3], p[4])

def thomasfermi2d(coords, *p):
    x, y = coords
#    print(p)
    n0, x_TF, y_TF, x0, y0 = p
    n = n0 * np.power(np.maximum(1 - ((x - x0) / x_TF)**2 - ((y - y0) / y_TF)**2, 0), 1.5)
    return n.ravel()

def guess_thomasfermi2d(x, y, f):
    i0 = np.unravel_index(f.argmax(), f.shape)
    x0 = x[i0[1]]; y0 = y[i0[0]]
    n0 = f[i0] - f.min()
    x_TF = 1# get_FWHM(x, map(integrate_equidistant, np.transpose(f)))
    y_TF = 1#get_FWHM(y, map(integrate_equidistant, f))
    
    p = np.array([n0, x_TF, y_TF, x0, y0])
    #print_tf_params(p)
    return p

def bimodal(coords, *p):
    pgauss = p[:6]
    ptf = extract_tf_p(p)
    return fit_gauss_2d.gauss2d(coords, *pgauss) + thomasfermi2d(coords, *ptf)

def fit_bimodal(x, y, f):
    c, v = curve_fit(bimodal, np.meshgrid(x, y), f.ravel(),
            p0 = np.append(fit_gauss_2d.guess_gauss2d(x, y, f), guess_thomasfermi2d(x, y, f)[:3]),
            bounds = (
                [0, 0, 0, -np.inf, -np.inf, -np.inf, 0, 0, 0], 
                [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf]
                    ))
    return c[:6], extract_tf_p(c)

def print_bimodal_params(p):
    fit_gauss_2d.print_params(p[:6])
    print_tf_params(p[6:])

def print_tf_params(p):
    print('n0 * max(1 - ((x - x0) / x_TF)^2  - ((y - y0) / y_TF)^2, 0)^(3/2)')
    print_eq(['n0', 'x_TF', 'y_TF', 'x0', 'y0'], p)
