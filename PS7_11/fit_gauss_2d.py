import numpy as np
from scipy.optimize import curve_fit

import density
import fit_gauss
from table import print_eq

def print_params(p, s="n0 * exp[-(x-x0)^2 / (2 sigma_x^2) - (y-y0)^2 / (2 sigma_y^2)] + c;\n"):
    print(s)
    print_eq(['n0', 'sigma_x', 'sigma_y', 'x0', 'y0', 'c'], 
            p,
            ['m^-2', 'm', 'm', 'm', 'm', 'm^-2'])

#   $n_0 \exp\left(- \frac{(x - x_0)^2}{2 \sigma_x^2} - \frac{(y - y_0)^2}{2 \sigma_y^2}] \right) + c$
def gauss2d(coords, *p):
    x, y = coords
    n0, sigma_x, sigma_y, x0, y0, c = p
    g = n0 * np.exp( - (x - x0)**2 / (2 * sigma_x**2)
            - (y - y0)**2 / (2 * sigma_y**2) ) + c
    return g.ravel()

def guess_gauss2d(x, y, f):
    i0 = np.unravel_index(f.argmax(), f.shape)
    x0 = x[i0[1]]; y0 = y[i0[0]]
    c = f.min()
    n0 = f[i0] - c
    sigma_x = 1/fit_gauss.F * fit_gauss.get_FWHM(x,
            map(density.integrate_equidistant, np.transpose(f)))
    sigma_y = 1/fit_gauss.F * fit_gauss.get_FWHM(y,
            map(density.integrate_equidistant, f))
    return np.array([n0, sigma_x, sigma_y, x0, y0, c])

def fit_gauss2d(x, y, f):
    c, v = curve_fit(gauss2d, np.meshgrid(x, y), f.ravel(), p0=guess_gauss2d(x, y, f))
    return c
