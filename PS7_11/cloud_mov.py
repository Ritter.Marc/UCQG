import numpy as np

import load_img
import density
import fit_gauss
import fit_gauss_2d
import constants as cst

def get_ypos(filenames):
    y0 = []
    for name in filenames:
        n = density.n_column(load_img.load_image(name), cst.SIGMA_RB)
        n_vert = density.n_vertical(n, cst.A_PIXEL)
        gp = fit_gauss.fit_gauss(cst.GRID_Y, n_vert)
        y0.append(-gp[2])
    return y0

def cloud_expansion(filenames):
    y0s = []
    sigmas = []
    for name in filenames: 
        n = density.n_column(load_img.load_image(name), cst.SIGMA_RB)
        gp = fit_gauss_2d.fit_gauss2d(cst.GRID_X, cst.GRID_Y, n)
        y0s.append(-gp[4])
        sigmas.append((gp[1], gp[2]))
    return y0s, sigmas
