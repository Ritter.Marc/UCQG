(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      5541,        166]
NotebookOptionsPosition[      4848,        140]
NotebookOutlinePosition[      5199,        155]
CellTagsIndexPosition[      5156,        152]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Load a binary image file (example)", "Subsection",
 CellChangeTimes->{{3.69028596466951*^9, 3.6902859712009025`*^9}, 
   3.6903109910165515`*^9, {3.7216691787722216`*^9, 3.72166918390613*^9}}],

Cell["First, define the variables used", "Text",
 CellChangeTimes->{{3.690285498252511*^9, 3.6902855083307543`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"filename", "=", "\"\<B_BEC_7.bin\>\""}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"resolutionX", "=", "200"}], ";"}], "   ", 
  RowBox[{"(*", " ", 
   RowBox[{"Avoid", " ", 
    RowBox[{"underscores", ":", " ", 
     RowBox[{
     "\"\<_\>\"", " ", "in", " ", "variables", " ", "in", " ", 
      "Mathematica"}]}]}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"resolutionY", "=", "200"}], ";"}]}], "Input",
 CellChangeTimes->{{3.690285414036646*^9, 3.690285435917475*^9}, {
  3.6902859094493976`*^9, 3.690285952043641*^9}, {3.6903109078729753`*^9, 
  3.690310922224807*^9}, {3.7220884210288095`*^9, 3.722088427825718*^9}, {
  3.725357431123371*^9, 3.7253574397797356`*^9}}],

Cell["\<\
We also need to make sure the current path is set to the path of the \
notebook, for example by using\
\>", "Text",
 CellChangeTimes->{{3.722088429560117*^9, 3.7220884470290804`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.7220884520448227`*^9, 3.722088455966752*^9}, {
  3.7220885064361267`*^9, 3.7220885334208384`*^9}}],

Cell[TextData[{
 "Now, define a function which loads an image given by the filename and \
resolution, and returns an array of numbers, with each pixel represented by \
one coefficient in the array / matrix. \nThe numbers are the ratio of the \
final intensity I(x,y) divided by the initial intensity ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["I", "0"], "(", 
     RowBox[{"x", ",", "y"}], ")"}], " "}], TraditionalForm]]],
 "of the light beam in the absorption imaging, so effectively  ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"data", "[", 
     RowBox[{"[", 
      RowBox[{"x", ",", "y"}], "]"}], "]"}], "=", 
    RowBox[{
     RowBox[{"I", "[", 
      RowBox[{"[", 
       RowBox[{"x", ",", "y"}], "]"}], "]"}], "  ", "/", "  ", 
     RowBox[{
      SubscriptBox["I", "0"], "[", 
      RowBox[{"[", 
       RowBox[{"x", ",", "y"}], "]"}], "]"}]}]}], TraditionalForm]]],
 "."
}], "Text",
 CellChangeTimes->{{3.6902856165584717`*^9, 3.6902857224304633`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"loadImage", "[", 
    RowBox[{"file_", ",", "resx_", ",", "resy_"}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"Transpose", "[", 
    RowBox[{"ArrayReshape", "[", 
     RowBox[{
      RowBox[{"BinaryReadList", "[", 
       RowBox[{"file", ",", "\"\<Real32\>\""}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"resx", ",", "resy"}], "}"}]}], "]"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.6902850889498715`*^9, 3.690285127258295*^9}, {
  3.690285165186841*^9, 3.690285196646101*^9}, {3.6902852448895955`*^9, 
  3.6902852603585787`*^9}, {3.690285371838832*^9, 3.6902854918305626`*^9}, {
  3.6902858432243195`*^9, 3.6902858828342237`*^9}}],

Cell["Now, actually load the file :", "Text",
 CellChangeTimes->{{3.6902857334774666`*^9, 3.6902857425244465`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"data", "=", 
   RowBox[{"loadImage", "[", 
    RowBox[{"filename", ",", "resolutionX", ",", "resolutionY"}], "]"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.690285748618208*^9, 3.690285765196585*^9}, {
  3.6902858899593067`*^9, 3.690285905746229*^9}}],

Cell["Plot the data for checking validity", "Text",
 CellChangeTimes->{{3.6902857677277946`*^9, 3.6902857829517584`*^9}}],

Cell[BoxData[
 RowBox[{"ArrayPlot", "[", "data", "]"}]], "Input",
 CellChangeTimes->{{3.690285361379201*^9, 3.6902853639620523`*^9}}],

Cell[TextData[{
 "Now, you can do the fitting of the data. In ",
 StyleBox["Mathematica",
  FontSlant->"Italic"],
 ", a typical function one would use is\n\[OpenCurlyDoubleQuote] \
NonlinearModelFit[...] \[OpenCurlyDoubleQuote]"
}], "Text",
 CellChangeTimes->{{3.6902859777790375`*^9, 3.69028600159188*^9}, {
  3.690286060606679*^9, 3.690286070341166*^9}}]
}, Open  ]]
},
WindowSize->{1252, 833},
WindowMargins->{{1025, Automatic}, {Automatic, 316}},
FrontEndVersion->"11.0 for Microsoft Windows (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 198, 2, 59, "Subsection"],
Cell[781, 26, 116, 1, 38, "Text"],
Cell[900, 29, 750, 18, 86, "Input"],
Cell[1653, 49, 193, 4, 38, "Text"],
Cell[1849, 55, 244, 5, 37, "Input"],
Cell[2096, 62, 1000, 28, 113, "Text"],
Cell[3099, 92, 704, 16, 62, "Input"],
Cell[3806, 110, 115, 1, 38, "Text"],
Cell[3924, 113, 289, 7, 37, "Input"],
Cell[4216, 122, 121, 1, 38, "Text"],
Cell[4340, 125, 133, 2, 37, "Input"],
Cell[4476, 129, 356, 8, 64, "Text"]
}, Open  ]]
}
]
*)

