function main()

    % Define my variables
    filename='A_Thermal_7.bin';
    resolution_x=300;  % number of pixels in x direction
    resolution_y=1000; % number of pixels in y direction

    % load data
    data=load_file(filename, resolution_x, resolution_y);

    % plot figure
    imagesc(data)

    % One- or Two-dimensional fitting in MATLAB: 
    % For example, use the function nlinfit(), or the function fit() 
    % (might require statistics or optimization toolbox)
    % or function fminsearch() (more general, and therefore slightly more complicated)
    
    % One- or Two-dimensional fitting in OCTAVE: 
    % Use the function leasqr()
    
end


%% Now define a function for loading images


% load_file returns the data in the form of a big matrix or array of size
% size_x * size_y, where each coefficient is the value of one pixel.
% These pixel values corresponds to the ratio of light intensity with atoms 
% (I(x,y) ) to light instensity without atoms (I_0(x,y) ):
% data(x,y)=I(x,y)/I_0(x,y)
function data=load_file(file, size_x, size_y)
    fid=fopen(file,'r','l');
    data=double(fread(fid,[size_y, size_x],'float'));
    fclose(fid);
end


