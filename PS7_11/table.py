__SAKUYA__ = object()

def get_table(table, sep=' ', init='\t'):
    strtable = [map(str, l) for l in zip(*table)]
    width = [max(map(len, col)) + 1 for col in zip(*strtable)]
    for line in strtable:
        yield(init + ''.join(
            [c.ljust(w, sep) for (c, w) in zip(line[:-1], width[:-1])] 
            + [line[-1]]))

def print_table(table, sep=' ', init='\t'):
    for line in get_table(table, sep, init):
        print(line)

def print_eq(col1, col2, units=__SAKUYA__):
    cols = [col1, ['='] * min(len(col1), len(col2)), col2]
    if units != __SAKUYA__:
        cols.append(units)
    print_table(cols)
