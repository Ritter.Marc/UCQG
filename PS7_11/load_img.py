import numpy as np

# Define the function which loads image file and returns content as a
# matrix (numpy array). The resulting data is the ratio of the 
# intensity with atoms divided by the intensity without atoms:
# imageData(x,y)=I(x,y)/I_0(x,y)

def load_image(filename, size_x = 300, size_y = 1000):
    with open(filename, 'rb') as fid:
        imageData = np.fromfile(fid, np.float32).reshape(size_x, size_y).T
        return imageData

def load_image_num(num):
    return load_image('data/A_Thermal_' + str(num) + '.bin')
