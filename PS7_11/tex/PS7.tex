\documentclass[10pt,a4paper,twoside]{article}
%\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}
%\usepackage[lighttt]{lmodern}
%\usepackage{lmodern}

\usepackage{fontspec}
\setmonofont{Inconsolata}

\usepackage{a4wide}
\usepackage{microtype}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{graphicx}
\usepackage{wrapfig}

\usepackage{minted}
\usemintedstyle{pastie}
\setminted[python]{bgcolor=black!3!white,linenos,mathescape,breaklines}

\usepackage{hyperref}

\title{Problem Set 7}

\newcommand{\git}{\texttt{git}}

\newcommand{\listc}[1]{
	\paragraph*{\detokenize{#1}}\hrulefill
	\inputminted{python}{../#1}
}

\newcommand{\ttline}[1]{\mint{bash}{#1}}
\newcommand{\ttser}[1]{\mintinline{bash}{#1}}
\newcommand{\tthead}[1]{\mintinline{#1}}

\newcommand{\getfig}[2][\linewidth]{
		\includegraphics[width=#1]{../pdf/figure_#2-crop}
}

\newcommand{\dd}{\mathrm{d}}
\DeclareMathOperator{\meter}{\mathrm{m}}
\DeclareMathOperator{\micrometer}{\mathrm{\mu m}}
\DeclareMathOperator{\millimeter}{\mathrm{m}}
\DeclareMathOperator{\millisecond}{\mathrm{ms}}
\DeclareMathOperator{\second}{\mathrm{s}}

\begin{document}\raggedbottom

\noindent Marc Ritter -- 11178889 -- Group~1 \hfill \today\\\hrule
\vspace{\baselineskip}
\begin{center}
{\LARGE Problem Set 7}
\end{center}

\section{Results}
\subsection{Problem 1}

\paragraph*{1a}
Lambert-Beer law:
\[
	d(x,y) \equiv \frac{I}{I_0}(x,y) = e^{-\sigma n(x,y)}
\]
therefore
\[
	n(x,y) = -\frac1\sigma \log(d(x,y))
\]
The column density is shown as a 2D plot in figure \ref{coldens}.

\begin{figure}[htbp]
	\centering
	\getfig[.5\linewidth]{1-1-zoom}
	\caption{Cloud column density $n(x,y)$ calculated from absorption image no. 7 (at $t = 12\millisecond$). Only the region directly surrounding the cloud is shown.}
	\label{coldens}
\end{figure}

\paragraph*{1b \& 1c}
See figure \ref{vertdens}. Fit parameters:
\[
	n_0 = 963.956 \cdot 10^6 \frac1\meter;	\quad
	\sigma_y = 22.636511 \micrometer;	\quad
	x_0 = 1.5056286 \millimeter;	\quad
	c = 31.1446 \cdot 10^6 \frac1\meter
\]

\begin{figure}[hbtp]
\centering
\begin{tabular}{ccc}
	\parbox{.4\linewidth}{
		\includegraphics[width=\linewidth]{../pdf/figure_1-2a-crop}
	}
	&\ &
	\parbox{.4\linewidth}{
		\includegraphics[width=\linewidth]{../pdf/figure_1-2b-crop}
	}
\end{tabular}
\caption{Vertical density $n(y) = \int\dd x ~ n(x,y)$ obtained by integration over column density and fit of a gaussian $n_0 \exp{-\frac{y^2}{2\sigma_y^2}} + c$. Left side: entire distribution, right side: relevant region around $y_0$.}
\label{vertdens}
\end{figure}

\begin{figure}[htbp]
	\centering
	\begin{tabular}{ccc}
		\parbox{.4\linewidth}{
			\getfig{parabola}
			\caption{blue: $y$-position of the cloud (in m) as a function of time (in seconds); orange: fit of a parabola $g t^2 + y_0$.}
			\label{parabola}
		}
		&\ &
		\parbox{.4\linewidth}{
			\getfig{2-1}
			\caption{Column density at $t=12\millisecond$ (colour plot) and fit of a 2D gaussian (white contour at 50\% and 95\% of the maximum.)}
			\label{fit2D}
		}
	\end{tabular}
\end{figure}

\paragraph*{1d \& 1e}
See figure \ref*{parabola}. Fit parameters for the parabola $y(t) = \frac{g}{2} t^2 + y_0$:
\[
	g = -9.77198 \frac{\meter}{\second^2};	\quad
	y_0 = - 0.790860 \meter
\]

\subsection{Problem 2}
\paragraph*{2a} See figure \ref{fit2D}.

\paragraph*{2b} See table \ref{y0fittable}. The maximum difference $\left|y_0^{\mathrm{1D}} - y_0^{\mathrm{2D}}\right|$ was 1.96591967289e-07 m.

\input{2bc}

\paragraph*{2c} See figure \ref{aspectratio}. A thermal distribution is isotropic in momentum space, therefore one would expect the atom cloud to quickly expand into a circular shape with $\sigma_x = \sigma_y$. In this case, however, the ellipticity $\sigma_x / \sigma_y$ moves away from 1. Therefore, the cloud must have been non-thermal before - either because of some external force which prevented thermalization, or because the gas was in a BEC state.

\begin{figure}[htbp]
	\centering
	\begin{tabular}{ccc}
		\parbox{.45\linewidth}{
			\getfig{2-2}
			\caption{The aspect ration $\sigma_x / \sigma_y$ of the 2d-gaussian fit as a function of time (in seconds).}
			\label{aspectratio}
		}
		&\ &
		\parbox{.45\linewidth}{
			\getfig{2-3}
			\caption{Widths $\sigma_x$ and $\sigma_y$ of the 2d gaussian (in metres) at times $t$ (in seconds) and fitted thermal expansion curves.}
			\label{temperaturefit}
		}
	\end{tabular}
\end{figure}

\paragraph*{2d} A cloud of atoms with gaussian distribution expands as $\sigma(t) = \sqrt{w_0^2 + \frac{k_B T}{m} t^2}$. For the fits, see figure \ref{temperaturefit}. The fit parameters yield a temperature of:
\[
	T_x = 1.25074664623\cdot10^{-8} K;
	\quad
	T_y = 2.21569595796\cdot10^{-8} K
\]

\paragraph*{2e} The number of atoms in a 2d gaussian distribution is given by
\[
	N = \int_{\mathbb{R}^2} \dd x\dd y ~ n(x,y) = 2\pi n_0 \sigma_x \sigma_y
\]
where $n_0$ is the peak density of the 2d gaussian, i.e. the peak column density. The atom number as a function of time is shown in figure \ref{atomnum}. The number of atoms in the cloud seems to be rising, which is probably because for a small cloud (at early times), the density is so high that the column density is saturated and thus ``cut off'', which leads to underestimation of $n_0$ at high densities. Only at later times $n_0$ is fitted correctly and one can estimate the atom number to be ca. 87000; more data is needed for more accurate estimation.

\begin{figure}[htbp]
	\centering
	\getfig[.5\linewidth]{2-4}
	\caption{Apparent number of atoms in the cloud at times $t$ (in seconds).}
	\label{atomnum}
\end{figure}

\setlength{\parindent}{0pt}

\section{Source}
\subsection{Get The Source}
The source is available as {\git}-repository via
\ttline{$ git clone https://gitlab.physik.uni-muenchen.de/Ritter.Marc/UCQG.git} %$
Alternatively, an archive containing the source can be obtained from
\url{https://gitlab.physik.uni-muenchen.de/Ritter.Marc/UCQG}
using any (sensible) webbrowser.

\subsection{Dependencies}
None which were not present in the example already.
\begin{enumerate}
	\item Python. This \emph{should} work on Python 2 as well as Python 3.\footnote{unless it doesn't... I used version 2.7.14.}%\footnote{The code has been tested on Python 2.7.14 and 3.6.3.}
	\item NumPy, SciPy and Matplotlib.
\end{enumerate}

\subsection{Usage}
Only files named \ttser{problem*.py} are executable as scripts:
\ttline{$ python problem*.py} %$

\subsection{Listings}
The complete source code, if you don't want to download it.

\listc{cloud_mov.py}
\listc{constants.py}
\listc{density.py}
\listc{fit_gauss.py}
\listc{fit_gauss_2d.py}
\listc{load_img.py}
\listc{7problem1abc.py}
\listc{7problem2a.py}
\listc{7problem2bc.py}
\listc{7problem2d.py}
\listc{7problem2e.py}
\listc{table.py}

\end{document}