from numpy import arange

M_RB = 87 * 1.66054E-27
A_PIXEL = 3.68E-6
SIGMA_RB = 2.9E-13
K_B = 1.380649E-23

WIDTH = 300
HEIGHT = 1000

T_MAX = 24E-3
NUM_FILES = 13
T = [T_MAX * i / (NUM_FILES - 1) for i in range(NUM_FILES)]
FILENAMES = ['data/A_Thermal_' + str(i) + '.bin' for i in range(1, NUM_FILES + 1)]

GRID_X = arange(WIDTH) * A_PIXEL
GRID_Y = arange(HEIGHT) * A_PIXEL
