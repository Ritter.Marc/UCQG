import numpy as np

#   $d = e^{-\sigma n}$
def n_column(d, crosssection):
    return -np.log(d)/crosssection

def n_vertical(n_column, a_pixel):
    return [integrate_equidistant(n, a_pixel) for n in n_column]

def n_total(n, pixel_area):
    return integrate_grid(n, pixel_area)

#   trapezoid
def integrate_equidistant(ydata, dx=1):
    return (ydata[0] + 2*sum(ydata[1:-1]) + ydata[-1]) * dx / 2

def integrate_grid(data, dA = 1):
    return integrate_equidistant(map(integrate_equidistant, data), dA)
