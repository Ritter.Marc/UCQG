import numpy as np
import matplotlib.pyplot as plt
import sys

import load_img
import density
import fit_gauss
import constants as cst

def main(argv):
    if len(argv) >= 2:
        num = argv[1]
    else:
        num = 7
    d = load_img.load_image_num(num)
    n = density.n_column(d, cst.SIGMA_RB)
    
    plt.imshow(n)
    plt.show()                                  # <- (a)
    
    n_vert = density.n_vertical(n, cst.A_PIXEL)     # <- (b)
    gp = fit_gauss.fit_gauss(cst.GRID_Y, n_vert)
    fit_gauss.print_params(gp)
    
    plt.plot(cst.GRID_Y, n_vert, cst.GRID_Y, fit_gauss.gauss(cst.GRID_Y, *gp))
                                                # <- (c)
    plt.show()

if __name__ == '__main__':
    main(sys.argv)
