from numpy import arange

M_RB = 87 * 1.66054E-27
A_PIXEL = 4.4E-6
SIGMA_RB = 2.9E-13
K_B = 1.380649E-23

WIDTH = 200
HEIGHT = 200

T = 13E-3
NUM_FILES = 46
#T = [T_MAX * i / (NUM_FILES - 1) for i in range(NUM_FILES)]
FILENAMES = ['data/B_BEC_' + str(i) + '.bin' for i in range(1, NUM_FILES + 1)]

GRID_X = arange(WIDTH) * A_PIXEL
GRID_Y = arange(HEIGHT) * A_PIXEL
