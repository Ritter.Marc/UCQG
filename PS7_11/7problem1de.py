import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

import cloud_mov
import constants as cst
from table import print_eq

def main():
    T2 = np.linspace(0, cst.T_MAX, 201)
    
    y0 = cloud_mov.get_ypos(cst.FILENAMES)
    c = list(curve_fit(lambda x, a, c: a*x**2 + c, 
            cst.T, y0, p0=(y0[1]/cst.T[1], y0[0]))[0])

    print_eq(['g', 'y0'], [c[0] * 2,  c[1]], ['m/s^2', 'm'])     # (g/2) * t**2

    plt.plot(cst.T, y0, T2, [c[0]*t**2 + c[1] for t in T2])
    plt.show()

if __name__ == '__main__':
    main()
