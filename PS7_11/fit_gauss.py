import numpy as np
from scipy.optimize import curve_fit

from table import print_eq

def print_params(p, s="a * exp[-(x-x0)^2 / (2 sigma^2)] + c;\n"):
    print(s)
    print_eq(['a', 'sigma', 'x0', 'c'], p, ['m^-1', 'm', 'm', 'm^-1'])

#   $a \exp\left(- \frac{(x - x_0)^2}{2 \sigma^2} \right) + c$
def gauss(x, *p):
    a, sigma, x0, c = p
    return c + a*np.exp( - (x - x0)**2 / (2 * sigma**2) )

def get_FWHM(x, f):
    peak = [x[i] for i in np.where(f > (max(f) + min(f)) / 2)[0]]
    if len(peak) >= 2:
        fwhm = abs(peak[-1] - peak[0])
        return fwhm if fwhm > 0 else 1
    else:
        return 1    

F = 2*np.sqrt(2*np.log(2))
def guess_gauss(x, f):
    i0 = np.argmax(f)
    x0 = x[i0]
    c = min(f)
    a = max(f) - c
    sigma = get_FWHM(x, f) / F
    return np.array([a, sigma, x0, c])

def fit_gauss(x, f):
    c, v = curve_fit(gauss, x, f, p0=guess_gauss(x,f))
    return c
