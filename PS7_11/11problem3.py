import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys

from load_img import load_image
import density
import fit_bimodal
import constants11 as cst
from fit_gauss_2d import gauss2d
from fit_gauss import get_FWHM

def Temp_from_slope(sigmax, sigmay):
    Tx = sigmax**2 * cst.M_RB / (cst.T**2 * cst.K_B)
    Ty = sigmay**2 * cst.M_RB / (cst.T**2 * cst.K_B)
    T = .5*(Tx + Ty)
    return T, np.sqrt((Tx-T)**2 + (Ty-T)**2)

def condensedfrac_th(T, *p):
    return np.maximum(1 - (T/p[0])**3, 0)

def main(files, PLOT = 0):
    cf = np.zeros(len(files))
    Ts = np.zeros(len(files))
    Terr = np.zeros(len(files))
    for fi in files:
        sys.stdout.write('\rfitting image {}/{}'.format(fi + 1, cst.NUM_FILES))
        sys.stdout.flush()

        image = load_image(cst.FILENAMES[fi], cst.WIDTH, cst.HEIGHT)
        n = density.n_column(image, cst.SIGMA_RB)

        n_vert = np.sum(n, 1)
        n_horz = np.sum(n, 0)

        maxi = (np.argmax(n_vert), np.argmax(n_horz))

#        maxi = np.unravel_index(n.argmax(), n.shape)
        
        width = 4 * get_FWHM(range(cst.WIDTH), n_horz)
        height = 4 * get_FWHM(range(cst.HEIGHT), n_vert)

        n = n[max(maxi[0] - height/2, 0) : min(maxi[0] + height/2, cst.HEIGHT),
                max(maxi[1] - width/2, 0) : min(maxi[1] + width/2, cst.WIDTH)]
        
        gridx = cst.GRID_X[maxi[1] - width/2 : maxi[1] + width/2]
        gridy = cst.GRID_Y[maxi[0] - height/2 : maxi[0] + height/2]

        cth, cBEC = fit_bimodal.fit_bimodal(gridx, gridy, n)

        NBEC = 2 * np.pi * cBEC[0] * cBEC[1] * cBEC[2] / 5.
        Nth = 2 * np.pi * cth[0] * cth[1] * cth[2]
        cf[fi] = NBEC / (Nth + NBEC)

        Ts[fi], Terr[fi] = Temp_from_slope(cth[1], cth[2])

        if(PLOT):
            tf = fit_bimodal.thomasfermi2d(np.meshgrid(gridx, gridy), *cBEC) \
                    .reshape(height, width)
            gauss = gauss2d(np.meshgrid(gridx, gridy), *cth) \
                    .reshape(height, width)

#        fit_bimodal.print_bimodal_params(np.append(cth, cBEC))

            plt.imshow(n,
                    extent=(gridx[0], gridx[-1], gridy[0], gridy[-1]),
                    origin='lower')
            plt.contour(gridx, gridy, gauss,
#                [(gauss.min() + gauss.max()) / 2, .95*(gauss.max() - gauss.min()) + gauss.min()],
                    colors='r', linestyles='dashed')
            plt.contour(gridx, gridy, tf,
#               [(tf.min() + tf.max()) / 2, .95*(tf.max() - tf.min()) + tf.min()],
                colors='w', linestyles='dotted')
            plt.show()
    print('\rfit complete            ')

    Tc = curve_fit(condensedfrac_th, Ts, cf, p0=[3e-7])[0]
    print(Tc)

    plt.errorbar(Ts, cf, xerr=Terr, fmt='k+')

    Tdense = np.linspace(min(Ts), max(Ts), 200)
    plt.plot(Tdense, condensedfrac_th(Tdense, Tc))
    plt.show()


if __name__ == '__main__':
    plot = '-p' in sys.argv
    main(range(cst.NUM_FILES), plot)
