import numpy as np
import matplotlib.pyplot as plt
import sys

import load_img
import density
import fit_gauss_2d
import constants as cst

def main(argv):
    if len(argv) >= 2:
        num = argv[1]
    else:
        num = 7
    d = load_img.load_image_num(num)
    n = density.n_column(d, cst.SIGMA_RB)
    
    gp = fit_gauss_2d.fit_gauss2d(cst.GRID_X, cst.GRID_Y, n)
    fit_gauss_2d.print_params(gp)
    fit = fit_gauss_2d.gauss2d(np.meshgrid(cst.GRID_X, cst.GRID_Y), *gp) \
            .reshape(cst.HEIGHT, cst.WIDTH)

    plt.imshow(n, extent=(cst.GRID_X[0], cst.GRID_X[-1], cst.GRID_Y[0], cst.GRID_Y[-1]),
            origin = 'lower')
    plt.contour(cst.GRID_X, cst.GRID_Y, fit, 
            [(fit.min() + fit.max())/2, fit.min() + .95*(fit.max() - fit.min())], 
            colors = 'w')
    plt.show()

if __name__ == '__main__':
    main(sys.argv)
