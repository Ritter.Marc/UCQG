import numpy as np
import matplotlib.pyplot as plt
import sys

from periodicpotential import pp_hamiltonian
import butler

def main(v, p=4, x=0, lmax=100):
    H = pp_hamiltonian(x, v, lmax)      # $H(\chi, \nu)$
    Es, cs = np.linalg.eigh(H)

    kxs = np.linspace(-1, 1, 1000)
    us = np.dot(np.exp(np.outer(2j*np.pi*kxs, range(-lmax, lmax))), cs[:,:p]) # $u_0^n (x) = \sum_l c_{l,0}^n \, e^{2li \, k \cdot x}$
    fig = plt.figure(figsize=(8.,8.))
    for i in xrange(len(us[0])):
        ax = fig.add_subplot(len(us[0]), 1, i+1)
        ax.set_title(r'$n = {}$'.format(i))
        line = ax.plot(kxs, np.real(us[:,i]), linestyle='-', label='real')[0]
        ax.plot(kxs, np.imag(us[:,i]), linestyle=':', color=line.get_color(), label='imag')
        ax.legend(loc=1)
        ax.grid()
        ax.set_xlabel(r'$x$'); ax.set_ylabel(r'$u_p^{} (x)$'.format(i))
    fig.suptitle(r'Bloch functions at $\nu={}, \chi={}$'.format(v, x))
    plt.tight_layout(rect=[0,.03,1.,.95])
    return fig

if __name__ == '__main__':
    p, k = butler.parseargv(sys.argv, 1, {'p' : 4, 'f' : '', 'x' : 0})
    fig = main(p[0], k['p'], k['x'])
    butler.saveorshow(k['f'], fig)
