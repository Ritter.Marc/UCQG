import numpy as np

def tridiagonal(t1, e, t2=None):
    return np.diag(t1, -1) + np.diag(e) + np.diag(t2 if t2 is not None else t1, 1)

def pp_hamiltonian(x, v, lmax=10):
    e = [(x + 2*l)**2 - v/2 for l in xrange(-lmax, lmax)]
    return tridiagonal([v/4] * (len(e) - 1), e)

def dispersionrelation(v, Nx=300, lmax=10):
    xs = np.linspace(-1, 1, Nx)
    Es = []
    for i in xrange(Nx):
        H = pp_hamiltonian(xs[i], v, lmax)
        Es.append(np.linalg.eigh(H)[0])
    return xs, Es
