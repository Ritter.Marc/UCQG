import numpy as np
import matplotlib.pyplot as plt
import sys

from periodicpotential import dispersionrelation
import butler

def main(p=3):
    vs = np.linspace(.1, 30, 300)
    bandwidths = []
    for v in vs:
        xs, Es = dispersionrelation(v, 3)
        bandwidths.append(np.absolute(Es[0][:p] - Es[1][:p])) # bandwidth $= \left|E(q=-k) - E(q=0)\right|$
    plt.semilogy(vs, bandwidths)
    plt.title('Dependence of Bandwidths on Lattice Depth')
    plt.xlabel(r'$\nu$'); plt.ylabel('Bandwidth')

if __name__ == '__main__':
    p, k = butler.parseargv(sys.argv, keys={'p': 3, 'f' : ''})
    main(k['p'])
    butler.saveorshow(k['f'])
