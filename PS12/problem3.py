import matplotlib.pyplot as plt

import bandstructure
import bandwidths
import blochfunctions

def path(name, val=''):
    return r'pdf/{}{}.pdf'.format(name, val)

for v in [.1,8.,25.,50.]:
    bandstructure.main(v).savefig(path('Bandstructure', v))

plt.figure()
bandwidths.main()
plt.savefig(path('Bandwidths'))

for v in [4, 40]:
    blochfunctions.main(v).savefig(path('Blochfunctions', v))
