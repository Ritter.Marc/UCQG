import matplotlib.pyplot as plt

def int_or_float(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s

def parseargv(argv, posp=0, keys={}, usage=None):
    if len(argv) < posp + 1:
        print('{} needs at least {} arguments.'.format(argv[0], posp))
        if usage is not None:
            print(usage)
        exit()
    
    p = []
    k = keys.copy()
    for arg in argv[1:posp+1]:
        p.append(int_or_float(arg))

    i = posp + 1
    while i < len(argv):
        if argv[i] not in keys:
            print('Ignored unknown parameter \'{}\''.format(argv[i]))
            i = i + 1
        elif len(argv) > i + 1 and not keys[argv[i]] is None:
            k.update({argv[i] : argv[i+1] if isinstance(keys[argv[i]], basestring) else int_or_float(argv[i+1])})
            i = i + 2
        else:
            k.update({argv[i] : True})
            i = i + 1
    return p, k

def saveorshow(filename, fig=None):
    if len(str(filename)) > 0:
        f = r'pdf/{}.pdf'.format(filename)
        if fig is None:
            fig = plt
        fig.savefig(f)
    else:
        plt.show()
