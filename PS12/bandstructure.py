import matplotlib.pyplot as plt
import sys

from periodicpotential import dispersionrelation
import butler

def main(v, p=5):
    xs, Es = dispersionrelation(v)
    fig = plt.figure()

    ax = plt.subplot(111)
    ax.plot(xs, [E[:p] for E in Es])
    ax.set_title(r'Bandstructure at $\nu = {}$'.format(v))
    ax.set_xlabel(r'$q$'); ax.set_ylabel(r'$E(q)$')
    return fig

if __name__ == '__main__':
    v, k = butler.parseargv(sys.argv, 1, {'p' : 5, 'f' : ''})
    fig = main(v[0], k['p'])
    butler.saveorshow(k['f'], fig)
